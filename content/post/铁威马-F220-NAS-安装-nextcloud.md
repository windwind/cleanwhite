---
title: "铁威马 F220 NAS 安装 nextcloud"
categories: [ "网络" ]
tags: [  ]
draft: false
slug: "41"
date: "2021-12-30 22:21:00"
---

铁威马 NAS 上可以用两种方式安装 nextcloud。第一种是在其应用中心直接安装 nextcloud 以及 MairaDB。第二中需要先安装docker，然后安装相应的 nextcloud 以及 MairaDB 容器。因为应用中心的 MairaDB 安装后无法正常启动，所以就使用容器进行安装。

在铁威马的 docker 管理界面中下载 nextcloud 以及 MairaDB 容器镜像。nextcloud 建议使用 MairaDB 从而获得更好的性能。但是在 nextcloud 初始化配置阶段链接 MairaDB 数据库可能会遇到下面错误。
```
General error: 4047 InnoDB refuses to write tables with ROW_FORMAT=COMPRESSED or KEY_BLOCK_SIZE.
```
这需要在 MairaDB 配置 `/etc/mysql/my.cnf`，在结尾添加 `innodb_read_only_compressed=OFF`。如果不想修改，也可以使用 `benjaminhu/mariadb_nextcloud:latest` 容器镜像（暂时只有 x86/amd64 格式），这里已经包含了上面的修改。
![docker-images.png][1]

**启动 MariaDB 容器**
容器名字 mariadb_db，可以任意取名。
![mariadb-name.png][2]

设置数据保存位置，使数据可以存储在容器之外，不会随容器删除而删除。
![mariadb-volum.png][3]

端口映射
![mariadb-port.png][4]

配置数据库 root 用户密码。这只需要给容器传递环境变量 `MYSQL_ROOT_PASSWORD` 即可。
![mariadb-env.png][5]

**启动 Nextcloud 容器**
指定一个容器名字。
![nextcloud-name.png][6]

配置下面三个目录，同样是数据保存在容器之外。
![nextcloud-vol.png][7]

映射端口，后面都将通过该端口登录。
![nextcloud-port.png][8]


链接容器，使得 nextcloud 容器有权访问该数据库容器。
![nextcloud-link.png][9]


最后使用浏览器登录IP:8080 即可看到 nextcloud 初始化界面。在 Storage 部分选择 mariadb 而非默认的 sqlite。依次输入数据库用户名 root, 刚才设置的密码 `MYSQL_ROOT_PASSWORD`，数据库名字 nextcloud_db，数据库直接配置为 db。这个就是先前配置的容器链接选项。

至此你应该可以使用 nextcloud 了。

  <!-- [1]: https://www.mazepark.xyz/usr/uploads/2021/12/407555761.png -->
  [1]: /img/407555761.png
  [2]: /img/2447110336.png
  [3]: /img/611588962.png
  [4]: /img/3297808510.png
  [5]: /img/699421844.png
  [6]: /img/1611700267.png
  [7]: /img/1958006041.png
  [8]: /img/2257686052.png
  [9]: /img/3060613016.png