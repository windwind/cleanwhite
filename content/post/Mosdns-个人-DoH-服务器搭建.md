---
title: "Mosdns 个人 DoH 服务器搭建"
categories: [ "网络" ]
tags: [ "mosdns","doh","dns" ]
draft: false
slug: "34"
date: "2021-09-09 16:10:00"
---

终究，终究还是上 DOH 服务器了。直接记录下这些过程吧。

首先准备证书。由于使用了非 443 端口，所以暂时放弃使用 Let's Encrypt，自己签发。当然最好的方法还是使用像 Let's Encrypt 这样公共的 CA 签发更好，服务器上 nginx 根据 URL 在转给不同业务端口。但真不想弄这么复杂。或者等哪天不得已的时候在上吧。下面命令生成证书和密钥。注意需要使用 SAN。
```
openssl req \
-x509 \
-newkey rsa:4096 \
-sha256 \
-days 3560 \
-nodes \
-keyout key.pem \
-out cert.pem \
-subj '/CN=YOUR_DNS_SERVER_NAME' \
-extensions san \
-config <( \
  echo '[req]'; \
  echo 'distinguished_name=req'; \
  echo '[san]'; \
  echo 'subjectAltName=DNS:YOUR_DNS_SERVER_NAME')
```

Server 端 mosdns 配置
```
log:
  level: info
  file: ""
library: []
plugin:
  - tag: server
    type: server
    args:
      entry:
        - forward_google
      server:
        - addr: '[::1]:53'
          protocol: udp
        - addr: '[::1]:53'
          protocol: tcp
  - tag: 'doh_server'
    type: 'server'
    args:
      entry:                 # 执行序列。该插件收到的请求将会运行这个序列
        - 'cache'
        - forward_google
      max_concurrent_queries: 0  # 插件最大并发处理请求数。默认 0 代表无限制。
      server:                    # 服务器设置。可配置多个。至少要配置一个。
        - protocol: 'doh'          # 必需。服务器协议。可以是 `udp`, `tcp`, `dot/tls`, `doh/https`, `http`。
          addr: '0.0.0.0:PORT'             # 必需。服务器监听地址。省略 IP 会监听所有地址(包括 IPv6)。
          timeout: 5               # 请求处理超时时间。单位: 秒。默认: 5。
          idle_timeout: 10         # 连接复用空连接超时时间。单位: 秒。默认: 10。适用于除了 `udp` 以外的协议。
          cert: '/PATH_TO/cert.pem' # `dot`, `doh` 必需。PEM 格式的证书文件。
          key: '/PATH_TO/key.pem'   # `dot`, `doh` 必需。PEM 格式的密钥文件。
          url_path: '/dns-query'    #url 路径。适用于 `doh`, `http`。如果为空，则任意路径的请求都能被处理。
  - tag: forward_google
    type: forward
    args:
      upstream:
        - addr: https://dns.google/dns-query
          ip_addr:
            - 8.8.8.8
            - 2001:4860:4860::8888
include: []
```

客户端 mosdns 配置。这里只配置了相应的插件，根据自己规则放进删除线效果。
```
  - tag: forward_remote               # 转发至远程服务器的插件
    type: forward
    args:
      upstream:
        - addr: https://YOUR_DNS_SERVER:PORT/dns-query
          trusted: true
      bootstrap:
        - https://223.5.5.5/dns-query
        - tls://1.1.1.1
```

除了直接用 MOSDNS 测试外，还可以用curl 命令对 DOH 服务先行简单测试。
```
curl --cacert cert.pem -H 'accept: application/dns-message' \
-v 'https://YOUR_DNS_SERVER:PORT/dns-query?dns=q80BAAABAAAAAAAAA3d3dwdleGFtcGxlA2NvbQAAAQAB' | \
hexdump
```

总的来说 MOSDNS 的配置还是比较简单。在路由器上跑占用的内存和 CPU 也很小。MOSDNS 已经用了有一年多，期间工作也相当稳定。
