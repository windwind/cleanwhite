---
title:       "SGTL5000 PCB 制作"
subtitle:    ""
description: ""
date:        2023-04-06
author:      "风舞山湖"
image:       ""
tags:        ["sgtl5000", "pcb", "嘉立创"]
categories:  ["野生技能" ]
---
这周需要做一块 SGTL5000 的板子，在嘉立创 EDA Pro 上选择了最便宜的 SGTL5000 芯片，封装是 20 Sawn QFN。焊盘的间距是 0.2mm。由于自己的焊接技术是在无法搞定这个封装，一开始想要直接在做板时选择下单贴片，但价格是实在是有点贵。特别是我只需要一到两块板即可。加工费已经超过材料费用不少了。所以最后决定买个加热焊台和中温锡膏，外加淘宝上定的小钢板，总费用低于贴片费。有了这些工具，后期自己做板也更加方便些。钢板制作费用加邮费共计 22 元。这对上锡膏帮助相当大。
![laser-panel][1]

上锡效果总体可以。一开始还担心锡太厚，特别是 SGTL5000 部分。最终加热后，电阻、电感爬锡很理想。SGTL5000 的部分焊盘的确粘连在一起。加点松香后可以用烙铁轻松清理干净。
![tin-pasting][2]
![sgtl5000-pin-connection][3]

在布好原件后，焊台上加热到 100 度，然后再调温度到 220 度，我使用的锡膏是 180 度，在 200 度时候开始融化。
{{< bilibili 397238774 >}}



  [1]:https://telegraph-image-cqn.pages.dev/file/1b6ab7226397f0dacc283.png
  [2]:https://telegraph-image-cqn.pages.dev/file/ac0fbed5829f2ad3af884.png
  [3]:https://telegraph-image-cqn.pages.dev/file/7971f3b7b7ac0b4d8fd54.png

