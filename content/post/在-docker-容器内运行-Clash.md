---
title: "在 docker 容器内运行 Clash"
categories: [ "网络" ]
tags: [ "rockpi4","clash","docerk" ]
draft: false
slug: "40"
date: "2021-12-16 17:48:00"
---

为什么要在容器内跑 clash 呢？因为好玩吧。之前入手了一个 Rock Pi 4,总想着怎么充分压榨它的资源，所有就想到了使用 docker，然后跑不同的容器，如本次的 clash，以及后续的 Trojan，Transmission 下载, WebDav。RK3399 CPU + 4G RAM + NVME SSD，这些足够撑起上面的应用。

首先用下面 dockerfile 自己构建一个容器 `docker build -t clash:1 .`

```
FROM debian:latest
RUN apt update
RUN apt-get install curl iptables -y
COPY clash-linux-armv8 /usr/bin/clash
WORKDIR /root/.config/clash
COPY Country.mmdb /root/.config/clash
COPY cache.db /root/.config/clash
WORKDIR /root/.config/clash/clash-dashboard
ADD clash-dashboard /root/.config/clash/clash-dashboard
WORKDIR /
COPY start_clash.sh /
ENTRYPOINT [ "/start_clash.sh" ]
```

这里在构建容器时已经将 clash 相关的文件，如 clash-linux-armv8，Country.mmdb， cache.db，clash-dashboard 以及启动脚本 start_clash.sh 都包含进来。上面这些文件都可以在 clash 的github 上下载到。start_clash.sh 如下：
```
#!/bin/bash
cp /opt/clash.yaml /root/.config/clash/config.yaml

/usr/sbin/iptables -t nat -N CLASH

# Bypass private IP address ranges
/usr/sbin/iptables -t nat -A CLASH -d 10.0.0.0/8 -j RETURN
/usr/sbin/iptables -t nat -A CLASH -d 127.0.0.0/8 -j RETURN
/usr/sbin/iptables -t nat -A CLASH -d 169.254.0.0/16 -j RETURN
/usr/sbin/iptables -t nat -A CLASH -d 172.16.0.0/12 -j RETURN
/usr/sbin/iptables -t nat -A CLASH -d 192.168.0.0/16 -j RETURN
/usr/sbin/iptables -t nat -A CLASH -d 224.0.0.0/4 -j RETURN
/usr/sbin/iptables -t nat -A CLASH -d 240.0.0.0/4 -j RETURN

# Redirect all TCP traffic to 8890 port, where Clash listens
/usr/sbin/iptables -t nat -A CLASH -p tcp -j REDIRECT --to-ports 7892
/usr/sbin/iptables -t nat -A PREROUTING -p tcp -j CLASH

/usr/bin/clash &

exec "$@"
```

start_clash.sh 主要提供 clash 配置文件 clash.yaml 和 iptables 配置规则，从而实现旁路由效果。

现在基本上提供的都是订阅地址，clash 是没法直接使用。可以使用 [subscribe2clash][1] 进行转换。

最后用下面的命令启动容器。/PATH-TO-CLASH 是包含 clash.yaml 的路径，需要挂载到容器的 /opt。
```
docker run -d -it --restart always --network macnet --ip 192.168.20.4 \
--privileged -v /PATH-TO-CLASH:/opt/ \
clash:1 bash
```


  [1]: https://github.com/whoisix/subscribe2clash