---
title:       "ESP8266 LED 灯带"
subtitle:    ""
description: ""
date:        2022-10-23
author:      "风舞山湖"
image:       ""
tags:        ["esp8266", "wled", "led"]
categories:  ["野生技能" ]
---
看到 [HACK 实验室][1]的一期使用 ESP 制作 Wi-Fi 控制的 LED 灯带视频，打算为家里的客厅也装一个。视频中使用到的开源固件是[WLED][2]，官网的介绍十分详细。除了固件编译和使用外，还有不少硬件电路设计的注意事项。后者对自行设计 PCB 的用户来讲是非常有用的。

家里客厅天花板的周长大约 10m。由于距离较长，无法使用 5V 供电，否则灯带上的损耗会导致严重的压降。因此，我选用了 12V 供电的灯带。实际测试也发现，即使采用 12V 供电，在 10m 灯带末端也不足 12V，LED 亮度肉眼可见的降低。如果是 15m 的话，那么最后的 5m 会由于严重的压降，导致 LED 显示颜色偏差。所以，如果使用较长的 LED 灯带，需要在灯带两端都连接 12V，甚至是多点注入 12V。这在 WLED 的设计文档里也提到了。另外控制器，如ESP8266，一个 GPIO 所控制的 LED 灯珠数量不要超过 800 颗。不然有些显示效果会有延时。

接下来使用 KiCAD 设计 PCB。整个设计其实十分简单。12V 到 5V 的降压电路，用于给 ESP8266 供电。另一部分为电平转换电路，用于 ESP8266 驱动 LED 灯带 WS2815。ESP8266 有开源的 KiCAD 封装 [kicad-ESP8266][4].
![schematic][3] 
![board.png][5]
PCB 加工我用了嘉立创，一周之内就到。12V 电源购买了成品方案，需要注意输出功率。我一共用了 600 颗灯珠。在白色全亮状态下大约 85W。

把 PCB 和电源都塞入天花板的角落。用热融胶把灯带固定。由于天花板上没有其他预留 220V 接口，只好拉个线下来。
![assemble.png][6]

看看效果吧。
{{< bilibili BV1yD4y1r7Mx >}}

我使用的[PCB 设计下载][7]。


  [1]: https://www.bilibili.com/video/BV12B4y1S7Za/?spm_id_from=333.999.0.0&vd_source=4473a38c1f603cd4b1da253267789a26
  [2]: https://github.com/Aircoookie/WLED
  [3]: /img/schematic.png
  [4]: https://github.com/jdunmire/kicad-ESP8266
  [5]: /img/board.png
  [6]: /img/assemble.png
  [7]: https://github.com/widewind2015/wled-esp8266-12v