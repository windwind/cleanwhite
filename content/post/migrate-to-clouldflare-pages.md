---
title:       "从 Typecho 迁移到 Cloudflare Pages"
subtitle:    ""
description: ""
date:        2022-03-15
author:      ""
image:       ""
tags:        ["cloudfalre", "Pages"]
categories:  ["网络" ]
---
年前在推特上看到有人介绍使用 Cloudflare Pages 搭配 Github/Gitlab 仓库搭建静态博客站点。可以利用 CI 功能自动生成文章，例如 Hugo。今天也终于把自己在 VPS 上搭建的 Typecho 站点给迁移过来。我也比较偏向用 markdown 离线编写文件，最自己熟悉的精简工具来表达内容。下面就记录下大致的过程。

借用[这篇文章][1]中提到的 [typecho-to-hugo][2] 工具，把原来的文章都导出为使用 markdown 语法的 md 文件。后续简单修改后就可以导入到 Hugo。

Cloudflare Pages 的注册可开通就不再这里重复。Cloudflare 的用户体验很不错，几乎凭直觉就可以进行下一步。在 Pages 中使用 Hugo 框架时需要注意版本。一般将 `HUGO_VERSION` 设置为最新的版本号，这可以方便使用 Hugo 主题。Cloudflare 将每个 Pages 站点视为一个项目，在创建项目时可以进行丰富的设置，选择 Github/Gitlab 仓库，从中获取网站数据。

Pages 项目默认采用 xxx.pages.dev 作为域名，xxx 是 Github/Gitlab 仓库名称。Pages 也可以使用用户自己设置的域名，这点实在很友善。这通过两种方法实现。第一种是直接由 Cloudflare 来托管一个域名，指向该 Pages 项目。另外如果你自己有其他 ISP 托管的域名，那么创建 CNAME 调试并指向 xxx.pages.dev。我自己有 DNSPOD 的域名，于是选择了第二种方式设置域名。

然后就选择一个 Hugo 主题，修改主题相关的一些配置。最后把之前导出的 md 文件放进来，例如 `content/post` 目录下。md 文件中所使用的图片放到 `static/img` 目录中。修改文件中所有图片的链接。如原来的 https://your.site/upload/xxx.png 替换为 /img/xxx.png。修改完成后可以提交到 Github/Gitlab 仓库。Cloudflare Pages 在检测到有新内容变更时，会自动重新编译。

Cloudflare Pages 默认就会使用其 CDN，所以在国内访问速度也还可以接受。我自己测试发现和原来在广州电信机房的 VPS 差距不大。使用 Github/Gitlab 存放文章和图片，也减轻了维护工作，不需要定期备份。

这样就释放了一台 VPS 资源。VPS 是去年双十一入手，还有两年使用期限。所以接下来就想想怎么继续利用这 VPS 了。


  [1]: https://eallion.com/typecho-to-hugo/
  [2]: https://github.com/eallion/typecho-to-hugo