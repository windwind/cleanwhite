---
title:       "在树莓派上播放 Spotify"
subtitle:    ""
description: ""
date:        2022-05-16
author:      "风舞山湖"
image:       ""
tags:        ["spotify", "树莓派"]
categories:  ["野生技能" ]
---
看着手边的树莓派 3B 1G 板子总是寻思着可以再压榨点什么，直到前不久看到了在它上面可以播放 Spotify。至于为什么不用手机蓝牙连接音响直接播放，主要原因是不想在播放时听到其他 App 的提示。除此之外，还可以解放手机。在树莓派上主要有两个任务需要完成。第一，添加外部音频解码器。第二，安装可以播放 Spotify 的软件 [raspotify][1] 。

### 添加外部音频解码器 ###
添加外部音频解码器，也称为外部声卡。通常可以由 USB 或者 I2S 通道和树莓派连接。I2S 比 USB 的延时更低，它是专门的音频协议。于是在海鲜市场上找到了一块 AOIDE Digi Pro。这是一块采用  WM8804G 的方案，还支持 S/PDIF。这个接口正好可以连接音响。

驱动方面，可以直接使用 Raspberry Pi OS 现成的 device tree overlays 和 WM8804G 相关内核驱动。在 `/boot/config.txt` 结尾添加 `dtoverlay=hifiberry-digi-pro`。并禁用树莓派 3B 本身的音频输出。在 `vc4-kms-v3d` 后面添加 `noaudio`。

```
# Enable DRM VC4 V3D driver
dtoverlay=vc4-kms-v3d,noaudio
...
[all]
dtoverlay=hifiberry-digi-pro
```

![P13B-AOLDE-DIGI-PRO.png][4]

### 安装 raspotify ###
安装 raspotify 也相当方便，运行下面命令即可。
```
sudo apt-get -y install curl && curl -sL https://dtcooper.github.io/raspotify/install.sh | sh
```

只要树莓派和手机在同一个网络中，那么手机上的 Spotify 可以看到该设备。
![spotify-select-device.png][2]

播放时直接选择 Raspotify。
![spotify-playback-over-device.png][3]

开始播放之后手机上的 Spotify 可以直接退出，树莓派则会继续播放。

{{< bilibili BV1st4y1x73k >}}


  [1]: https://github.com/dtcooper/raspotify
  [2]: /img/spotify-select-device.png
  [3]: /img/spotify-playback-over-device.png
  [4]: /img/P13B-AOLDE-DIGI-PRO.png