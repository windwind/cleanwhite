---
title: "Visual Studio Code Remote SSH 远程连接"
categories: [ "嵌入式" ]
tags: [ "vscode","ssh","linux","yocot" ]
draft: false
slug: "33"
date: "2021-04-16 14:50:43"
---

之前看到一个 2021 年流行的编程集成环境 IDE 排行没有提到 Visual Studio Code 而只有 Visual Studio。这引起了好事网友的一些疑问，为什么没有 Visual Studio Code。作者的回答是 Visual Studio Code 只是一个文本编辑器。这确实挺客观的解释。Visual Studio Code 可以安装合适的插件来编译和调试各类语言。而我首次用 Visual Studio Code 的时候也确实把它当作一款 Linux 中优秀的文本编辑器。而后才开始用于 Python 学习。最近发现 Visual Studio Code 安装 Remote SSH 后，可以非常方便在远程服务器执行编译和配置工作。

最早在编译服务器上安装的是 Ubuntu desktop 系统，使用 VNC 远程登录。随着 Yocto Project 愈发庞大，内存开始变得越来越紧张。远程 VNC 连接也会时不时会发生崩溃。于是就关闭图形界面，使用 SSH + Vim。但都这个年代了，应该可以有比各种需要组合键操作的更人性化方案。Visual Studio Code + Remote SSH 就是我想要的。

首先当然是要安装 Remote SSH，这是巨硬家的亲儿子插件。
![remote-ssh.png][1]

点击 VS Code 左边的图标。
![ssh-login.png][2]

首次使用的话需要新建目标设备的 SSH 连接。
![new.png][3]

在 Linux 系统推荐使用 ~/.ssh/config 文件来管理 SSH 目标节点。Visual Studio Code 可以解析该文件，并支持其中 SSH 跳转设置。这样点不得不说很优秀。这个文件甚至可以复制出来，在 Windows 环境中使用。
![cfg.png][4]

在 SSH 节点列表上右击选择 Connect to host in current window 或者 connect to host in new window 即可连接到目标主机。连接成功后会默认打开一个终端。

光是 SSH 远程链接肯定不能展现 VS Code 实力。点击 File -> Open file 或者 Open Folder 来打开远程主机上的一个文件或者目录。
![open.png][5]

打开目录时可以选择下一级子目录。
![open-folder.png][6]

如果要同时打开多个目录可以选择 Add Folder to Workspace. 打开后就可以像本地文件一样直接进行编辑。
![open-remote.png][7]

打开多个终端可以在 Terminal 中选择 New bash. 多个终端只能切换，而不能同时以 panel 形式显示。或许我没有找到合适的方法。
![new-bash.png][8]

下载文件也可以通过右击 Download 完成。上传功能我还没有找到。
![download.png][9]

打开 Visual Studio Code 显示一堆代码，他不一定是在编程。

  [1]: /img/2703647763.png
  [2]: /img/426325021.png
  [3]: /img/2972548838.png
  [4]: /img/3619837428.png
  [5]: /img/132141216.png
  [6]: /img/3373118805.png
  [7]: /img/3444739719.png
  [8]: /img/668006135.png
  [9]: /img/641286041.png