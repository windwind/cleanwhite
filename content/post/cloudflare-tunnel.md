---
title:       "无需公网 IP 访问内网站点 - Cloudflare Tunnels"
subtitle:    ""
description: ""
date:        2022-08-27
author:      "风舞山湖"
image:       ""
tags:        ["Tunnels", "Cloudflare", "公网IP"]
categories:  ["网络" ]
---
对于访问在内网的站点，通常可以使用 DDNS，VPN 隧道或者在拥有公网 IP 的服务器做反向代理。在没有公网 IP 的情况下采用最多的是 DDNS，如NoIP，花生壳。Cloudfalre Tunnel 提供了另外一种无需公网 IP 访问内网资源的途径。

Cloudfalre Tunnel 工作方式如下。Cloudfalre 托管一个网站，通常只是一个域名的 DNS 解析。例如 yoursite.com，其中一个是 tunnel.yoursite.com。在 Cloudfalre Tunnel 创建一个隧道，绑定域名 tunnel.yoursite.com。在一个内网服务器上运行后台程序 cloudflared，由其提供 tunnel.yoursite.com 所需的业务，如 HTTPS/SSH/TCP 等连接 。当用户访问 tunnel.yoursite.com 时，Cloudfalre 的云服务会自动从对应隧道的 cloudflared 获取需要的数据并将其提供给访问的用户。
![cf-tunnel-how-it-works.png][1]

第一步点击 Cloudfalre Dash 的 Zero Trus。点开 Access -> Tunnels，点击右边的 Create a tunnel，输入一个隧道的名称。
![cf-create-tunnel.png][2]

第二步，根据内网服务器的操作系统和 CPU 类型，复制自动生成的命令，并在服务器上运行。运行成功后，服务器与 Cloudfalre Tunnel 就建立了连接。Tunnel 目前已经支持了 Win/Mac/Debian/Docker 多种系统。CPU 也涵盖了 X86，Arm64，Arm32。
![cf-tunnel-install-connector.png][3]

第三步，点击建立隧道的 Public Hostname 标签页。点击右边的 add a public hostname。
![cf-tunnel-publich-hostname.png][4]

Subdomain 是该隧道的子域名。Domain 下来菜单中选择在 Cloudflare 托管的站点。Service 是对应内网服务器提供的业务类型。:// 后面的 URL 是内网服务器上业务侦听的地址和端口。点击 Save hostname 后即可。 
![cf-add-public-hostname.png][5]

此时，在 DNS 记录里可以看到一条 CNAME 解析，UUID.cfargotunnel.com 被指向了刚才设置的隧道子域名。

Cloudfalre Tunnel 能在没有公网 IP 的情况下建立一个隧道。同时可以避免客户端直接连接内网服务器。数据交互通过 Cloudfalre 的云服务器中转，所以连接速度可能受到网络环境的限制。




  [1]: /img/cf-tunnel-how-it-works.png
  [2]: /img/cf-create-tunnel.png
  [3]: /img/cf-tunnel-install-connector.png
  [4]: /img/cf-tunnel-publich-hostname.png
  [5]: /img/cf-add-public-hostname.png

