---
title:       "使用 Fluentbit, Grafan, InfluxDB 搭建设备监控平台"
subtitle:    ""
description: ""
date:        2022-04-10
author:      "风舞山湖"
image:       ""
tags:        ["fluentbit", "grafana", "influxdb"]
categories:  ["网络" ]
---
自从 RockPi 4B 开始担任 All-IN-ONE 旁路由系统已接近一年。期间的运行很稳定，最近决定为其添加最后一个功能，设备监控。这包括 CPU、温度、内存这些基础信息。选择的方案是 Fluentbit, Grafan, InfluxDB 组合。

Fluentbit 是一个很灵活的工具，可以很简单地获取 CPU、温度、内存信息。通过插件还可以监控其他应用，例如容器运行状态等。RockPi 4B 主要跑各种容器，所以这个功能也是非常有用的。但在写这篇文章时，我还没有测试这个功能。Fluentbit 在输出捕获的信息时也十分灵活。可以直接保存为本地文件，或者通过 HTTPS 推送，以及可以直接对接例如 InfuxDB 数据库，格式方面也支持 JSON。

首先在 RockPi 4B 上安装 Fluentbit，执行命令 `sudo apt-get install td-agent-bit`。在 /etc/td-agent-bit/td-agent-bit.conf 中添加三个 `INPUT` 选项，分别用于获取 CPU、温度、内存信息，获取间隔为 5 秒。`Tag` 标签用于 InfuxDB 数据库检索。
```
[INPUT]
    Name cpu
    Tag  rockpi_cpu
    Interval_Sec 5

[INPUT]
    Name   mem
    Tag    rockpi_memory
    Interval_Sec 5

[INPUT]
    Name thermal
    Tag  rockpi_thermal
    Interval_Sec 5
```

然后配置 Fluentbit 的输出。`Match` 为 `*`，表示将上面所有的 `INPUT` 捕获的信息都由该 `OUTPUT` 处理。这里都会被发送到 InfluxDB 数据库。`Host`，`Port` 为 InfluxDB 数据库的地址（IP 或者域名）和端口。`Bucket`，`Org`，`HTTP_User`，`HTTP_Passwd` 是在配置 InfluxDB 数据库相关信息。`HTTP_Token` 密钥可以在配置好的数据库里查看。接下来马上介绍这部分内容。
```
[OUTPUT]
    Name  influxdb
    Match *
    Host 192.168.3.117
    Port 8086
    Bucket grafana
    Org grafana
    HTTP_User grafana
    HTTP_Passwd XXXXXX
    HTTP_Token XXXXXXXXXXXXXXXX
```


在服务器端，直接用下面 docker-compose.yml 来启动 Grafan 和 InfluxDB。
```
version: "3"
services:
  grafana:
    image: grafana/grafana
    container_name: grafana
    ports:
      - 3000:3000
    networks:
      - grafana_network
    volumes:
      - grafana-storage:/var/lib/grafana
    depends_on:
      - influxdb
    deploy:
      resources:
        limits:
          memory: 500M

  influxdb:
    image: influxdb:latest
    container_name: influxdb
    ports:
      - 8086:8086
    networks:
      - grafana_network
    volumes:
      - influxdb-storage:/var/lib/influxdb
    environment:
      - INFLUXDB_DB=grafana
      - INFLUXDB_USER=grafana
      - INFLUXDB_USER_PASSWORD=XXXXXX
      - INFLUXDB_ADMIN_ENABLED=true
      - INFLUXDB_ADMIN_USER=admin
      - INFLUXDB_ADMIN_PASSWORD=XXXXXX
    deploy:
      resources:
        limits:
          memory: 1300M

networks:
  grafana_network:

volumes:
  influxdb-storage:
  grafana-storage:
```
InfluxDB 似乎会持续消耗内存，所以这里限制其最多使用 1.3GB 内存。启动两个容器后，首先登陆 InfluxDB 管理后台 192.168.3.117:8086。创建 bucket 和 organization 均为 grafana。这里的取名和上面 td-agent-bit.conf 中的设置一致即可。完成后在 Data -> API Token 中可以看到用于 Fluentbit 的 HTTP_Token。
![influxdb_api_token.png][1]

在 RockPi 4B 上运行下面命令，InfluxDB 即可接收到数据。
``` sudo systemctl restart td-agent-bit```

完成数据库配置后，接下来登录到 Grafana 后台链接 InfluxDB。InfluxDB 的 URL 地址可以使用上面其容器的名字 influxdb。
![Grafana-Influxdb-1.png][2]
![Grafana-Influxdb-2.png][3]
![Grafana-Influxdb-3.png][4]

最后在 Dashboard 中分别创建三个 panel 用于展示 CPU、温度和内存信息。
![Grafana-Influxdb-cpu-query.png][5]

查询语法如下：

**CPU**
```
from(bucket: "grafana")
  |> range(start: -30m)
  |> filter(fn: (r) =>
    r._measurement == "rockpi_cpu" and
    r._field == "cpu_p"
  )
  |> drop(columns: ["_seq"])
```

**温度**
```
from(bucket: "grafana")
  |> range(start: -30m)
  |> filter(fn: (r) => r["_measurement"] == "rockpi_thermal")
  |> filter(fn: (r) => r["_field"] == "temp")
  |> drop(columns: ["_seq"])
```

**内存**
```
from(bucket: "grafana")
  |> range(start:-30m)
  |> filter(fn: (r) => r["_measurement"] == "rockpi_memory")
  |> filter(fn: (r) => r["_field"] == "Mem.used")
  |> drop(columns: ["_seq"])
```

Grafana 和 InfluxDB 运行在另外一个 Arm 板子上，内存为 2GB，所以上面限制了容器可用的最大内存。在使用了几天之后，发现 influxdb 常会在耗尽内存后崩溃，所以将容器设置自动重启。由于 volume 并没有被删除，所有容器重启后之前的数据仍旧存在，RockPi 4B 上也无需重新配置。


  [1]: /img/influxdb_api_token.png.png
  [2]: /img/Grafana-Influxdb-1.png
  [3]: /img/Grafana-Influxdb-2.png
  [4]: /img/Grafana-Influxdb-3.png
  [5]: /img/Grafana-Influxdb-cpu-query.png










